---
title: "Utilidades Gerais"
date: 2021-10-24T18:01:31-03:00
draft: false
---

## Como realizar a manobra clássica de Heimlich (usada em caso de engasgos)
1. Posicione-se atrás da vítima, envolvendo-a com os braços;
2. Feche uma das mãos, com o punho bem fechado e o polegar por cima, e posicione-a na região superior do abdômen, entre o umbigo e a caixa torácica;
3. Coloque a outra mão sobre o punho fechado, agarrando-o firmemente;
4. Puxe com força ambas as mãos para dentro e para cima, como se desenhasse um "c" ou uma vírgula com o punho. Caso essa região seja de difícil acesso, como pode acontecer em obesos ou gestantes nas últimas semanas, uma opção é posicionar as mãos sobre o tórax;
5. Repita a manobra até 5 vezes seguidas, observando se o objeto foi expelido e se a vítima respira.

Na maioria das vezes, estes passos são suficientes para que o objeto seja expelido, entretanto, em alguns casos, a vítima pode continuar sem conseguir respirar corretamente e acabar desmaiando. Neste caso, deverá ser feita a manobra adaptada para a pessoa desmaiada.

adaptado de https://www.tuasaude.com/manobra-de-heimlich/

## Como fazer ovo frito com gema mole
1. tire os ovos da geladeira algum tempo antes de começar o preparo
2. coloque óleo da preferência suficiente para cubrir a superfície da panela(de preferência anti-aderente)
3. ligue o fogo baixo
4. quebre o ovo em uma superfície plana e coloque em algum recipiente
5. verifique se o ovo está em bom estado
6. coloque o ovo na panela
7. coloque algumas gotas de água e cubra com uma panela
8. coloque sal, pimenta do reino e o ingrediente que desejar
9. quando a clara ficar toda branca e consistente, o ovo pode ser retirado
10. o ponto da gema pode ser escolhido a partir disso. 