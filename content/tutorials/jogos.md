---
title: "Jogos"
date: 2021-10-24T17:36:40-03:00
draft: false
---

## Como fazer uma casa submersa no minecraft

1. colete muitos blocos de argila
2. colete plantas aquáticas para dar um charme
3. faça baldes 
4. pegue lava
5. delimite com blocos o espaço da sua casa que fica submerso
6. utiliza a lava para eliminar a agua rapidamente
7. utilize os baldes para retirar a agua restante
8. decore o resto
9. Pronto! uma casa submersa



## Como jogar verdade ou desafio

1. tenha uma garrafa ou algum objeto que possa apontar para duas pessoas
2. gire a garrafa ou objeto
3. uma das pessoas apontada pergunta ( no caso da garrafa é em quem parou a base da garrafa)
4. a outra responde (em quem para a tampa)
5. quem pergunta começa com : "verdade ou desafio"?
6. a pessoa que responde decide uma opção e diz para o grupo
7. a pessoa que pergunta deve fazer um desafio ou pergunta para a outra.
o objetivo normalmente é ser algo engraçado, vergonhoso ou dúvida interessante
8. após a pergunta ser respondida de maneira satisfatória, ou o desafio ser cumprido, 
a garrafa é girada novamente
9. caso não seja cumprido a opção, o grupo pode decidir uma prenda de acordo com o que foi 
requisitado


